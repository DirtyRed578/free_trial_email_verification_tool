'use strict';

var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var del = require('del');
var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');

// Set the browser that you want to support
const AUTOPREFIXER_BROWSERS = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
];

// Gulp task to minify CSS files
gulp.task('styles', function () {
    return gulp.src('./css/*.css')
        // Auto-prefix css styles for cross browser compatibility
        .pipe(autoprefixer({Browserslist: AUTOPREFIXER_BROWSERS}))
        // Minify the file
        .pipe(csso())
        // Output
        .pipe(gulp.dest('./dist/css'))
});

// Gulp task to move JavaScript files
gulp.task('scripts', function() {
    return gulp.src('./js/app.js',)
        // Output
        .pipe(gulp.dest('./dist/js'))
});

// Gulp task to move img files
gulp.task('img', function() {
    return gulp.src('./img/**/*')
        // Output
        .pipe(gulp.dest('./dist/img'))
});

// Gulp task to move php pages
gulp.task('php', function() {
    return gulp.src(['./*.php'])
        // move to dist
        .pipe(gulp.dest('./dist/'));
});

// Gulp task to move prod vue
gulp.task('vue_prod', function() {
    return gulp.src(['./js/vue_prod/vue.js'])
        // move to dist
        .pipe(gulp.dest('./dist/js'));
});

// Gulp task to move prod vue
gulp.task('html', function() {
    return gulp.src(['./*.html'])
        // move to dist
        .pipe(gulp.dest('./dist'));
});

// Clean output directory
gulp.task('clean', () => del(['dist']));

// Gulp task to minify all files
gulp.task('default', ['clean'], function () {
    runSequence(
        'styles',
        'scripts',
        'img',
        'php',
        'vue_prod',
        'html'
    );
});
