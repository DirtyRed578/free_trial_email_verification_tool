<?php
session_start();

// local? show errors
if($_SERVER['SERVER_ADDR'] == '::1'){
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
} else {
	error_reporting(0);
}

// rate limiter
if( isset($_SESSION['LAST_CALL']) ){
    $l = strtotime($_SESSION['LAST_CALL']);
    $c = strtotime(date("Y-m-d h:i:s"));
    $sec =  abs($l - $c);
    // 5 seconds per post
    if ($sec <= 5) {
        header('Content-Type: application/json');
        die('{"err": true, "msg": "Internal Server Rejection, Rate limit exceeded."}');
    }
}
$_SESSION['LAST_CALL'] = date("Y-m-d h:i:s");

// model
require_once('trialVerifier.php');

$jsonReturn = array(
	"invalid" => '{"err": true, "msg": "Invalid Email Address."}',
	"exists" => '{"err": false, "loc": "./exists.html"}',
	"freetrial" => '{"err": false, "loc": "https://pro.creativemarket.com/sign-up"}',
	"servererror" => '{"err": true, "msg": "Internal Server Error, Please Try Your Request Again."}'
);

 /*
	@param $email: (string) email
	@return: (boolean) true if email is valid, false if email is not valid
	uses built in php filters to sanitize then check if supplied email is valid
*/
function isEmailValid($email)
{
	$sanitized = filter_var($email, FILTER_SANITIZE_EMAIL);
	return filter_var($sanitized, FILTER_VALIDATE_EMAIL);
}

// business logic
if( !isEmailValid($_POST['email']) ){
	// invalid email
	echo $jsonReturn["invalid"];
} else {
	$ftv = new FreeTrailVerifier();
	$email = $_POST['email'];

	// check if email exists
	if($ftv->doesEmailExist($email)){
		// email already exists
		echo $jsonReturn["exists"];
	} else {
		// attempt to add to table
		$succ = $ftv->postEmail($email, $_SERVER['REMOTE_ADDR']);
		if(!$succ){
			// something went wrong when trying to add to our table
			echo $jsonReturn["servererror"];
		} else {
			// user succesfully added - can redirect to sign up page
			echo $jsonReturn["freetrial"];
		}
	}
}

?>