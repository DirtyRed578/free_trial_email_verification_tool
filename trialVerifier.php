<?php
class FreeTrailVerifier
{
    private $db;

    public function __construct()
    {
        date_default_timezone_set('America/Los_Angeles');

        $database = 'freetrialverification';

        // remote creds if needed
        $host = '';
        $user = '';
        $pass = '';

        // local
        if($_SERVER['SERVER_ADDR'] == '::1'){
        	$isWindows = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
        	// differentiating between my mac and windows dev envs.
            if( !$isWindows ){
              $host = 'localhost:8889';
              $user = 'root';
              $pass = 'root';
            } else {
              $host = 'localhost:3308';
              $user = 'root';
              $pass = '';
            }
        }

        try{
            $this->db = new PDO('mysql:host='.$host.';dbname='.$database, $user, $pass);
        } catch (Exception $e) {
            die('ERROR: CANNOT CONNECT TO MYSQL');
        }
    }

    /*
		@param $email: (string) email
		@return: (int) 1 if email exists, 0 if email does not exist
		checks if email exists in the email table
    */
    public function doesEmailExist($email)
    {
        $query = "SELECT COUNT('id') FROM emails WHERE email = :email";
        $statement = $this->db->prepare($query);
        $statement->bindValue(':email', $email);
        $statement->execute();
        $r = $statement->fetch(PDO::FETCH_NUM)[0];
       	return $r;
    }

    /*
		@param $email: (string) email
		@param $ip: (string) ipv4 ip address
		@return: (boolean) true if successful, false if not
		adds email,ip, and current timestamp to table. Called after a NEW person has signed up for a free trial
    */
    public function postEmail($email,$ip)
    {
    	$query = "INSERT INTO emails (email, ip) VALUES (:email, :ip)";
    	$statement = $this->db->prepare($query);
    	$statement->bindValue(':email', $email);
    	$statement->bindValue(':ip', $ip);
        return $statement->execute();
    }
}
?>