var app = new Vue({
	el: '#emailForm',
	data: {
		email: "",
		error: ""
	},
	methods: {
		// regex front end email validator
		isValidEmail: function(){
			if( /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(this.email) ){
				return true;
			} else {
				return false;
			}
		},
		sendemail: function(){
			// only do a network request if email is at least 4 char long and valid email
			if(this.email.length > 4 && this.isValidEmail()){
				const ajax = new XMLHttpRequest();
				ajax.open('POST', 'control.php', true);
				ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				let that = this;
				ajax.onload = function(){
					let response = "";
					try{
						// attempt to parse response from server as json, otherwise expect a server side error
						response = JSON.parse(ajax.responseText);
					}catch(e){
						console.log(e);
						response = {"err": true, "msg": "Internal Server Error, Please Try Your Request Again."};
					}
					// if server is good and there is a response, check if there is an error
					if(response.err){
						that.error = response.msg;
					} else {
						// if no error then just simply redirect
						window.location = response.loc;
					}
				};
				ajax.send("email="+this.email);
			} else {
				// show error if email is not at least 4 char long and not valid
				this.error = "Invalid email address";
			}

			// hide error message after 2.5 seconds
			let that = this;
			setTimeout(function(){
				that.error = '';
			},2500)
		}
	}
});